<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    protected $table = 'settings';

    protected $fillable = ['user_id', 'meta_key', 'meta_value', 'is_active', 'script_id', 'script_path'];

    protected $casts = [
        'meta_value' => 'array',
        'is_active' => 'boolean',
    ];
}
