<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Osiset\ShopifyApp\Storage\Models\Plan;

class TestDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $x = User::all();
//        dd($x->toJson());
        $a = Plan::find(1);
        $a->test = false;
        $a->save();
        dd($a->toJson());
        return Command::SUCCESS;
    }
}
