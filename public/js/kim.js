//Create the element using the createElement method.
let scrollCss = '.imomo-scroll-top {display: inline-block; background-color: #FF9800; width: 50px; height: 50px; text-align: center; border-radius: 4px;' +
        'position: fixed; bottom: 30px; right: 30px; transition: background-color .3s, opacity .5s, ' +
        'visibility .5s; z-index: 1000; cursor: pointer; padding: 5px;}' +
    '.imomo-scroll-top:hover {background: red}' +
    '.imomo-scroll-top.bottom-right {bottom: 50px; right: 50px;}' +
    '.imomo-scroll-top.bottom-left bottom: 50px; right: auto; left: 50px;}' +
    '.imomo-scroll-top.center-right {bottom: 50%; right: 50px;}' +
    '.imomo-scroll-top.center-left {bottom: 50%; right: auto; left: 50px;}' +
    '.imomo-icon {position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);}';
let style = document.createElement('style');
style.appendChild(document.createTextNode(scrollCss));
document.getElementsByTagName('head')[0].appendChild(style);

let myDiv = document.createElement("div");
myDiv.className = 'imomo-scroll-top';
//Add your content to the DIV

myDiv.innerHTML = '<svg class="imomo" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><polygon fill="#fff" points="396.6,352 416,331.3 256,160 96,331.3 115.3,352 256,201.5 "></polygon></svg>';

//Finally, append the element to the HTML body
document.body.appendChild(myDiv);

