<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['verify.shopify', 'billable']], function () {
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('settings', [\App\Http\Controllers\SettingController::class, 'getSetting']);
    Route::post('settings', [\App\Http\Controllers\SettingController::class, 'saveSetting']);
});

Route::get('/privacy-policy', function () {
    return view('privacy');
});

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

Route::get('scripts/{id}/{version}.js', function ($id) {
    $setting = \App\Models\Setting::find($id);
    return response()->view('scripts', compact('setting'))->header('Content-Type', 'application/javascript');
})->name('script_tag');
