// Get the element
let topBtn = document.querySelector(".imomo-scroll-top");

// On Click, Scroll to the page's top, replace 'smooth' with 'instant' if you don't want smooth scrolling
topBtn.onclick = () => window.scrollTo({ top: 0, behavior: "smooth" });

// On scroll, Show/Hide the btn with animation
window.onscroll = () => window.scrollY > 300 ? topBtn.style.opacity = 1 : topBtn.style.opacity = 0
